// - Создать простую HTML страницу с кнопкой `Вычислить по IP`.
// - По нажатию на кнопку - отправить AJAX запрос по адресу `https://api.ipify.org/?format=json`, получить оттуда IP адрес клиента.
// - Узнав IP адрес, отправить запрос на сервис `https://ip-api.com/` и получить информацию о физическом адресе.
// - Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.
// - Все запросы на сервер необходимо выполнить с помощью async await.

const btnCheckIp = document.querySelector('#btn-check-ip')
const ipWrapper = document.querySelector('.ip')

async function getIp() {
    const ip = await fetch ("https://api.ipify.org/?format=json")
    const ipJson = await ip.json()
    return ipJson
}

async function showIp() {
    clear()
    const ip = await getIp()
    const ipInfo = document.createElement('div')
    ipInfo.className= 'ip-info'
    console.log(ipInfo);
    ipInfo.innerText = `Your IP is: ${ip.ip}`
    ipWrapper.append(ipInfo)
 
    await getData(ip.ip)

}

async function getData(ip) {
    const data = await fetch (`http://ip-api.com/json/${ip}`)
    const dataJson = await data.json()
     showInfo(dataJson) 
}

  async function showInfo(data) {
    const ipInfo = await document.querySelector('.ip-info')
    await ipInfo.insertAdjacentHTML ('beforeend', `
    <p> Your country is: ${data.country}; Your region is: ${data.regionName}; Your city is: ${data.city}; Your timezone is: ${data.timezone} </p>
    `)
}

function clear(){
    const info = document.querySelectorAll('.ip-info')
    if (info.length > 0) {
        info.forEach(element => {
          element.remove();
        })};
}
btnCheckIp.addEventListener('click', () => {
      showIp()
})


